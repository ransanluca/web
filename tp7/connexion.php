<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Connexion</title>
  </head>
  <body>
    <h1>Portail de connexion</h1>

    <?php if (isset($_GET["erreur"])): ?>
      <p>
        Il y a une erreur de <?php echo $_GET["erreur"] ?>.
      </p>
    <?php endif; ?>

    <form action="verifierConnexion.php" method="POST">
      <ul>
        <li>
          <label for="pseudo">Pseudo : </label>
          <input id="pseudo" name="pseudo">
        </li>

        <li>
          <label for="mdp">Mot de passe : </label>
          <input id="mdp" name="mdp">
        </li>

        <li>
          <input type="submit" value="Valider">
        </li>
      </ul>
    </form>

    <p>
      <a href="inscriptionEleves.php">Inscription en tant qu'élève</a>
    </p>
    <p>
      <a href="inscriptionProfs.php">Inscription en tant que professeur</a>
    </p>
  </body>
</html>
