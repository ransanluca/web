<?php
class BDD {
    private $fichier;
    public $class_name;

    public function __construct($class_name, $nom_fichier) {
        $this->fichier = fopen($nom_fichier, "a+");
        $this->class_name = $class_name;
    }

    public function put($personne) {
        flock($this->fichier, LOCK_EX);
        fputcsv($this->fichier, $personne->get_array(), ";");
        flock($this->fichier, LOCK_UN);
    }

    public function get_all() {
        flock($this->fichier, LOCK_SH);
        rewind($this->fichier);
        $objs = [];
        while (($line = fgetcsv($this->fichier, 0, ";"))) {
            if ($line[0] !== null) {
                $objs[] = new $this->class_name($line);
            }
        }
        flock($this->fichier, LOCK_UN);
        return $objs;
    }

    public function get($field, $value) {
        $cls = $this->get_all();
        $filtered = array_filter($cls, function($v) use ($field, $value) {
            return $v->{$field} === $value;
        });
        return array_values($filtered)[0];
    }
}

abstract class Personne {
    public function __construct($a) {
        $i = 0;
        foreach (get_object_vars($this) as $key => $val) {
            $this->{$key} = $a[$i];
            $i++;
        }
    }

    public function get_array() {
        $a = [];
        foreach (get_object_vars($this) as $val) {
            $a[] = $val;
        }
        return $a;
    }
}

class Eleve extends Personne {
    public $nom;
    public $prenom;
    public $naissance;
    public $adresse;
    public $points;
    public $pseudo;
    public $mdp;
}

class Prof extends Personne {
    public $nom;
    public $prenom;
    public $matiere;
    public $pseudo;
    public $mdp;
}

class Admin extends Personne {
    public $nom;
    public $prenom;
    public $pseudo;
    public $mdp;
}

$eleves_db = new BDD("Eleve", "eleves.csv");
$profs_db = new BDD("Prof", "profs.csv");
$admins_db = new BDD("Admin", "admins.csv");
?>
