<?php
require_once("outils.php");

$eleve = new Eleve([
    $_POST["nom"],
    $_POST["prenom"],
    $_POST["naissance"],
    "ADRESSE",
    "POINTS",
    $_POST["pseudo"],
    $_POST["mdp"],
]);
$eleves_db->put($eleve);
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Inscription élèves</title>
  </head>
  <body>
    <h1>Inscription élèves</h1>

    <p>
      L'élève <?php echo $_POST["prenom"] . " " . $_POST["nom"] ?>
      a bien été enresistré !
    </p>
    <p>
      <a href="inscriptionProfs.php">Rajouter un élève</a>
    </p>
    <p>
      <a href="connexion.php">Retour à la page de connexion</a>
    </p>
  </body>
</html>
