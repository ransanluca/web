<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Inscription professeurs</title>
  </head>
  <body>
    <h1>Inscription professeurs</h1>

    <form action="enregistrerProfs.php" method="POST">
      <ul>
        <li>
          <label for="nom">Nom : </label>
          <input id="nom" name="nom">
        </li>

        <li>
          <label for="prenom">Prénom : </label>
          <input id="prenom" name="prenom">
        </li>

        <li>
          <label for="matiere">Matiere : </label>
          <input id="matiere" name="matiere">
        </li>

        <li>
          <label for="pseudo">Pseudo : </label>
          <input id="pseudo" name="pseudo">
        </li>

        <li>
          <label for="mdp">Mot de passe : </label>
          <input id="mdp" name="mdp">
        </li>

        <li>
          <input type="submit" value="Valider">
        </li>
      </ul>
    </form>
  </body>
</html>
