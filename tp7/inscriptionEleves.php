<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Inscription élèves</title>
  </head>
  <body>
    <h1>Inscription élèves</h1>

    <form action="enregistrerEleves.php" method="POST">
      <ul>
        <li>
          <label for="nom">Nom : </label>
          <input id="nom" name="nom">
        </li>

        <li>
          <label for="prenom">Prénom : </label>
          <input id="prenom" name="prenom">
        </li>

        <li>
          <label for="naissance">Date de naissance : </label>
          <input type="date" id="naissance" name="naissance">
        </li>

        <li>
          <label for="pseudo">Pseudo : </label>
          <input id="pseudo" name="pseudo">
        </li>

        <li>
          <label for="mdp">Mot de passe : </label>
          <input id="mdp" name="mdp">
        </li>

        <li>
          <input type="submit" value="Valider">
        </li>
      </ul>
    </form>
  </body>
</html>
