<?php
session_start();
require_once("outils.php");

if (!isset($_SESSION["pseudo"])) {
    header("Location: connexion.php");
    exit();
}
$user = $_SESSION["pseudo"];
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Accueil</title>
  </head>
  <body>
    <h1>Accueil</h1>

    <p>
      Bienvenue <?php echo $_SESSION["prenom"] ?> !
    </p>

    <?php if ($_SESSION["type"] === "admin"): ?>
      <table>
        <?php foreach ($eleves_db->get_all() as $eleve): ?>
          <tr>
            <?php foreach ($eleve->get_array() as $val): ?>
              <td>
                <?php echo $val ?>
              </td>
            <?php endforeach; ?>
          </tr>
        <?php endforeach; ?>
      </table>
    <?php endif; ?>

    <form method="POST" action="deconnexion.php">
      <input type="submit" name="OUT" value="Déconnexion">
    </form>
  </body>
</html>
