<?php
session_start();
require_once("outils.php");

function check_password($user, $type) {
    if ($_POST["mdp"] === $user->mdp) {
        $_SESSION["type"] = $type;
        foreach (get_object_vars($user) as $key => $val) {
            $_SESSION[$key] = $val;
        }
        header("Location: accueil.php");
    } else {
        header("Location: connexion.php?erreur=mdp");
    }
    exit();
}

$eleve = $eleves_db->get("pseudo", $_POST["pseudo"]);
if ($eleve)
    check_password($user, "eleve");

$prof = $profs_db->get("pseudo", $_POST["pseudo"]);
if ($prof)
    check_password($prof, "prof");

$admin = $admins_db->get("pseudo", $_POST["pseudo"]);
if ($admin)
    check_password($admin, "admin");

header("Location: connexion.php?erreur=pseudo");
?>
