<?php
require_once("outils.php");

$prof = new Prof([
    $_POST["nom"],
    $_POST["prenom"],
    $_POST["matiere"],
    $_POST["pseudo"],
    $_POST["mdp"],
]);
$profs_db->put($prof);
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Inscription professeurs</title>
  </head>
  <body>
    <h1>Inscription professeurs</h1>

    <p>
      Le professeur <?php echo $_POST["prenom"] . " " . $_POST["nom"] ?>
      a bien été enresistré !
    </p>
    <p>
      <a href="inscriptionProfs.php">Rajouter un professeur</a>
    </p>
    <p>
      <a href="connexion.php">Retour à la page de connexion</a>
    </p>
  </body>
</html>
