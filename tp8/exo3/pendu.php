<?php
session_start();

if (isset($_GET["letter"])) {
    $letter = $_GET["letter"];
    $word = $_SESSION["word"];
    $letters = $_SESSION["letters"];
    unset($letters[array_search($letter, $letters)]);
} else {
    $words = file("mots.txt", FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
    $word = $words[array_rand($words)];
    $_SESSION["word"] = $word;
    $letters = str_split("abcdefghijklmnopqsrtuvwxyz");
}

$_SESSION["letters"] = $letters;
echo str_replace($letters, "_", $word);
?>
