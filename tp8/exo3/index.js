window.onload = main

function main() {
    "abcdefghijklmnopqrstuvwxyz"
        .split("")
        .forEach(l => {
            let button = document.createElement("button")
            button.textContent = l
            button.onclick = letter_click;
            let li = document.createElement("li")
            li.appendChild(button)
            document.querySelector("#letters").appendChild(li)
        })

    {
        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = () => {
            if (xhttp.readyState === XMLHttpRequest.DONE && xhttp.status === 200) {
                document.querySelector("#word").textContent = xhttp.responseText
            }
        }
        xhttp.open("GET", "pendu.php")
        xhttp.send()
    }
}

function letter_click() {
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = () => {
        if (xhttp.readyState === XMLHttpRequest.DONE && xhttp.status === 200) {
            document.querySelector("#word").textContent = xhttp.responseText
            this.disabled = true
            document.querySelector("#tries").textContent--
        }
    }
    xhttp.open("GET", `pendu.php?letter=${this.textContent}`)
    xhttp.send()
}
