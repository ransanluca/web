"use strict"

let retournees = 0
let dernieres = []

window.onload = main

function main() {
    const table = document.querySelector("table")

    const cartes = []
    for (let i = 1; i <= 10; i++) {
        cartes.push(i)
        cartes.push(i)
    }
    shuffleArray(cartes)

    for (let i = 0; i < 4; i++) {
        const row = table.insertRow()
        for (let j = 0; j < 5; j++) {
            const td = document.createElement("td")
            const img = document.createElement("img")
            td.appendChild(img)
            row.appendChild(td)

            img.dataset.n = cartes[i * 5 + j]
            img.dataset.found = false
            img.onclick = click
            img.src = "img/fond.png"
            cartes[i * 4 + j] = img
        }
    }
}

function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}

function click() {
    if (this.dataset.found === "false") {
        if (retournees % 2 === 0) {
            if (dernieres.length === 2)
                dernieres.forEach(x => x.src = "img/fond.png")
            this.src = `img/carte${this.dataset.n}.png`
            dernieres[0] = this
            retournees++
        } else if (this !== dernieres[0]) {
            this.src = `img/carte${this.dataset.n}.png`
            if (this.dataset.n === dernieres[0].dataset.n) {
                this.dataset.found = dernieres[0].dataset.found = true
                dernieres.length = 0
                retournees++
            } else {
                dernieres[1] = this
                retournees--
            }
        }
        if (retournees === 20) {
            alert("fini!")
        }
    }
}
