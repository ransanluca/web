"use strict"

const noms = [
    'Jurassic Parc',
    'Star Wars',
    'Hobbit',
    'Cite Or',
    'Le dernier Empereur',
    'Apocalypse now', 
]

let magasin, choix, prix
let total = 0

window.onload = main

function main() {
    magasin = document.getElementById("magasin")
    choix = document.getElementById("choix")
    prix = document.getElementById("prix")
    Array.from(document.getElementsByTagName("img"))
        .forEach(i => {
            i.onclick = deplacer
        })
}

function deplacer() {
    if (this.parentNode === magasin) {
        magasin.removeChild(this)
        choix.appendChild(this)
        total += 2.5
    } else if (this.parentNode === choix) {
        choix.removeChild(this)
        magasin.appendChild(this)
        total -= 2.5
    } else {
        console.log("impossible")
    }
    prix.textContent = `${Math.floor(total)}€${(total % 1) * 10}0`
}
