"use strict"

let select

window.onload = main

function main() {
    select = document.querySelector("select")
}

function ajouter_artiste() {
    const artiste = document.getElementById("ajouter").value
    if (artiste !== "") {
        const o = document.createElement("option")
        o.text = artiste
        select.appendChild(o)
    }
}

function supprimer_artiste() {
    const artiste = document.getElementById("supprimer").value
    if (artiste !== "") {
        Array.from(select.options)
            .filter(o => o.text === artiste)
            .forEach(o => {
                select.removeChild(o)
            })
    }
}
