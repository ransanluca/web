"use strict"

const kitty = "kitty.jpg"
const pirates = "pirates.jpg"
const images = ["pirates.jpg", "forrest.jpg", "starwars.jpg"]

function img_onmouseover() {
    img.src = kitty
}

function img_onmouseout() {
    img.src = pirates
}

function img_onclick() {
    alert(`source: ${img.src}
taille: ${img.naturalWidth}x${img.naturalHeight}`)
}

function btn_onclick() {
    img.src = images[Math.floor(Math.random() * images.length)]
}
