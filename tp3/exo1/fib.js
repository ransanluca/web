"use strict"

function fib(n) {
    let a = 0n
    let b = 1n
    for (let i = 0; i < n; i++) {
        let c = a
        a += b
        b = c
    }
    return a
}

const n = prompt("saisissez un rang")
const d = new Date()

window.onload = display

function display() {
    document.getElementById("ici").innerHTML =
        `À ${d.getHours()}h${d.getMinutes()},<br>
le ${n}ᵉ terme de la suite de Fibonacci est <b>${fib(n)}</b>.`
}

