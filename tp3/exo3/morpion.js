"use strict"

const images = Array.from(Array(3), () => new Array(3))
const grille = Array.from(Array(3), () => new Array(3))
let joueur = 1

window.onload = main

function main() {
    const table = document.querySelector("table")
    for (let i = 0; i < 3; i++) {
        const row = table.insertRow()
        for (let j = 0; j < 3; j++) {
            const td = document.createElement("td")
            const img = document.createElement("img")
            img.onclick = () => img_onclick(i, j)
            td.appendChild(img)
            row.appendChild(td)

            images[i][j] = img
        }
    }
    reset()
}

function img_onclick(i, j) {
    console.log(`clicked on ${i}, ${j}`)
    if (grille[i][j] === 0) {
        images[i][j].src = `fond${joueur}.png`
        grille[i][j] = joueur
        if (grille.every(l => l.every(x => x !== 0))) {
            alert(`Partie nulle`)
            reset()
        } else if (gagne()) {
            alert(`Joueur ${joueur} a gagné`)
            reset()
        } else {
            joueur_suivant()
        }
    } else {
        console.log("can't play on this case")
    }
}

function joueur_suivant() {
    joueur = joueur === 1 ? 2 : 1
}

function gagne() {
    for (const line of grille)
        if (line.every(x => x === joueur))
            return true
    for (let j = 0; j < 3; j++) {
        const column = grille.map(l => l[j])
        if (column.every(x => x === joueur))
            return true
    }
    const diags = [
        [grille[0][0], grille[1][1], grille[2][2]],
        [grille[0][2], grille[1][1], grille[2][0]],
    ]
    for (const diag of diags)
        if (diag.every(x => x === joueur))
            return true
    return false
}

function reset() {
    joueur = 1
    images.forEach(l => l.forEach(i => i.src = "fond.png"))
    grille.forEach(l => l.fill(0))
}
