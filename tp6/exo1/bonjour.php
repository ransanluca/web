<?php
class Joueur {
    public $nom;
    public $prenom;
    public $naissance;
    public $poste;

    public function __construct($nom, $prenom, $naissance, $poste) {
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->naissance = date_create($naissance);
        $this->poste = $poste;
    }

    public function age() {
        return date_create()->diff($this->naissance)
                            ->y;
    }
}
$joueurs = [
    new Joueur("Giroud", "Olivier", "1986.09.30", "Attaquant"),
    new Joueur("Griezman", "Antoine", "1991.03.21", "Attaquant"),
];
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Gazon</title>
  </head>
  <body>
    <h1>Bonjour le monde !</h1>
    <p>On est le <?php echo date("Y-m-d \à H:i:s.") ?></p>

    <table>
      <tr>
        <th>Nom</th>
        <th>Prénom</th>
        <th>Date de naissance</th>
        <th>Poste</th>
        <th>Âge</th>
      </tr>
      <?php foreach ($joueurs as $j): ?>
        <tr>
          <td><?php echo $j->nom; ?></td>
          <td><?php echo $j->prenom; ?></td>
          <td><?php echo $j->naissance->format("Y-m-d"); ?></td>
          <td><?php echo $j->poste; ?></td>
          <td><?php echo $j->age(); ?></td>
        </tr>
      <?php endforeach; ?>
    </table>
  </body>
</html>
