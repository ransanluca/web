"use strict"

window.onload = main

function main() {
    const url_params = new URLSearchParams(window.location.search)
    for (const [k, v] of url_params) {
        const ul = document.querySelector("#real")
        const li = document.createElement("li")
        li.textContent = `${k} => ${v}`
        ul.appendChild(li)
    }
}
